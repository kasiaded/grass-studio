class Lightbox {
  constructor() {
    this.lightboxTrigger = document.querySelectorAll(".lightbox-trigger");
    this.lightbox = document.querySelector(".lightbox");
    this.lightboxCross = document.querySelector(".lightbox__cross");
    this.lightboxImage = document.querySelector(".lightbox__image");
    this.bodyElement = document.querySelector("body");
    this.clickOnImage();
    this.clickOnCross();
  }

  clickOnImage() {
    this.lightboxTrigger.forEach((element) => {
      element.onclick = (event) => {
        this.addVisibleClass();
        this.addSrcToLightbox(event);
      };
    });
  }

  clickOnCross() {
    this.lightboxCross.onclick = () => {
      this.removeVisibleClass();
    };
  }

  addSrcToLightbox(event) {
    this.lightboxImage.src = event.target.src;
  }

  addVisibleClass() {
    this.lightbox.classList.add("visible");
    this.bodyElement.classList.add("stop-overflow");
  }
  removeVisibleClass() {
    this.lightbox.classList.remove("visible");
    this.bodyElement.classList.remove("stop-overflow");
  }
}

const lightbox = new Lightbox();

export default Lightbox;
