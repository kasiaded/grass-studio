class Submenu {
  constructor() {
    this.menuElements = document.querySelectorAll(".header__list-item");
    this.listenToClick();
  }

  listenToClick() {
    this.menuElements.forEach((element) => {
      element.onclick = (event) => {
        this.checkSubmenuClass(event);
      };
    });
    document.onclick = (event) => {
      const listElement = event.target.nextElementSibling;
      if (
        !listElement ||
        (listElement && !listElement.classList.contains("submenu"))
      ) {
        const submenuTarget = document.querySelector(".submenu.visible");
        if (submenuTarget) {
          this.removeClassToSubmenu(submenuTarget);
        }
      }
    };
  }

  addClassToSubmenu(submenuTarget) {
    submenuTarget.classList.add("visible");
  }

  removeClassToSubmenu(submenuTarget) {
    submenuTarget.classList.remove("visible");
  }

  checkSubmenuClass(event) {
    event.preventDefault();
    const submenuTarget = event.target.nextElementSibling;
    if (submenuTarget) {
      if (submenuTarget.classList.contains("visible")) {
        this.removeClassToSubmenu(submenuTarget);
      } else {
        this.addClassToSubmenu(submenuTarget);
      }
    }
  }
}

const submenu = new Submenu();

export default Submenu;
